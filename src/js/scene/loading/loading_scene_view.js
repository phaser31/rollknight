import { EventEmitter } from "events";
import { Assets } from "../../data/game_assets";

export default class LoadingSceneView {

    /**
     * @param  {Phaser.Scene} scene
     */
    constructor(scene) {
        this.scene = scene;

        this.container = this.scene.add.container();

        this.event = new EventEmitter();
		this.eventNames = {
		}

        this.centerX = this.scene.scale.width * .5;
        this.centerY = this.scene.scale.height * .5;
        this.width = this.scene.scale.width;
        this.height = this.scene.scale.height;

        this.animKey = {
            run: 'run',
            loadingDust: 'loadingDust'
        }

    }

    create = () => {
        this.background = this.scene.add.image(this.centerX, this.centerY, Assets.loading.images.menu.key);
        const backgroundWidth = this.height * (this.background.width / this.background.height);
        this.background.setDisplaySize(backgroundWidth, this.height);

        this.darkBackground = this.scene.add.rectangle(this.centerX, this.centerY, this.width, this.height, "#000000");
        this.darkBackground.setAlpha(.25);

        this.playerAnim = this.scene.anims.create({
            key: this.animKey.run,
            frames: Assets.loading.spritesheets.playerRun.key,
            frameRate: 12,
            repeat: -1,
        });
        this.player = this.scene.add.sprite(this.centerX, this.centerY, Assets.loading.spritesheets.playerRun.key, 0);
        const playerWidth = this.width * .9
        this.player.setDisplaySize(playerWidth, (playerWidth) * (this.player.height/this.player.width));
        this.player.anims.play(this.animKey.run);

        this.dustAnim = this.scene.anims.create({
            key: this.animKey.loadingDust,
            frames: Assets.loading.spritesheets.dust.key,
            frameRate: 12,
            repeat: -1,
        });
        this.dust = this.scene.add.sprite(this.centerX, this.centerY, Assets.loading.spritesheets.dust.key, 0);
        const dustWidth = this.player.displayWidth * .3
        this.dust.setPosition(this.player.x - this.player.displayWidth * .4, this.player.y + this.player.displayHeight * .2);
        this.dust.setFlipX(true);
        this.dust.setDisplaySize(dustWidth, (dustWidth) * (this.dust.height/this.dust.width));
        this.dust.anims.play(this.animKey.loadingDust);

        this.text = this.scene.add.text(this.player.x, this.player.y + this.player.displayHeight * .5, "0%", {
            fontSize: this.player.displayHeight * .2,
            color: "#FFFFFF",
            fontFamily: Assets.loading.fonts.origamiMommy.key
        });
        this.text.setAlpha(.75);
        this.text.setOrigin(.5);
    }

    removeAnimation = () => {
        this.scene.anims.remove(this.animKey.loadingDust);
        this.scene.anims.remove(this.animKey.run);
    }

    setText = (text) => {
        this.text.setText(`${text}%`);
    }
}