import { Assets } from "../../data/game_assets";
import { Scene_Info } from "../../data/game_scene";
import AssetLoader from "../../module/asset_loader";
import LoadingSceneView from "./loading_scene_view";

export default class LoadingSceneController extends Phaser.Scene {
    constructor() {
        super({
            key: Scene_Info.loading
        });
    }

    preload = () => {
        this.load.once('complete', () => {
            this._loadGameResource();
        });

        const imageAssets = Object.values(Assets.loading.images);
        imageAssets.forEach(image => {
            AssetLoader.LoadImage(this, image);
        });

        const fontAssets = Object.values(Assets.loading.fonts);
        Promise.all(fontAssets.map(font => { AssetLoader.LoadFont(font.key, font.path) }))
            .then(() => {
                console.log('font load success')
            })
            .catch(error => {
                console.log(error);
            });

        const spritesheetAssets = Object.values(Assets.loading.spritesheets);
        spritesheetAssets.forEach(spritesheet => {
            AssetLoader.LoadSpriteSheet(this, spritesheet);
        });

        this.load.start();
    }

    _loadGameResource = () => {
        this.view = new LoadingSceneView(this);
        this.view.create();

        this.load.on('progress', (value) => {
            this.view.setText(Math.round(value * 100))
        });
        this.load.once('complete', () => {
            this.view.removeAnimation();
            this.scene.start(Scene_Info.mainMenu);
        });

        const imageAssets = Object.values(Assets.images);
        imageAssets.forEach(image => {
            AssetLoader.LoadImage(this, image);
        });

        const spritesheetAssets = Object.values(Assets.spritesheets);
        spritesheetAssets.forEach(spritesheet => {
            AssetLoader.LoadSpriteSheet(this, spritesheet);
        });

        this.load.start();
    }
}