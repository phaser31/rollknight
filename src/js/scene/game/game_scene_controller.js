import { Assets } from '../../data/game_assets';
import BoardController from './board/board_controller';
import game_config from '../../data/game_config';
import BallSpawnerController from './ball_spawner/ball_spawner_controller';
import InputController from './input/input_controller';
import BlockItemSpawnerController from './block_item_spawner/block_item_spawner_controller';
import StartSceneView from './game_scene_view';
import { Scene_Info } from '../../data/game_scene';

export default class GameSceneController extends Phaser.Scene {
    constructor() {
        super({
            key: Scene_Info.game
        });
    }

    create = () => {
        
    }

    registerEvent = () => {
        
    }
}