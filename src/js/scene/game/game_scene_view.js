import { EventEmitter } from "events";
import { Assets } from "../../data/game_assets";

export default class GameSceneView {

    /**
     * @param  {Phaser.Scene} scene
     */
    constructor(scene) {
        this.scene = scene;

        this.container = this.scene.add.container();
        this.gameoverContainer = this.scene.add.container();

        this.event = new EventEmitter();
		this.eventNames = {
		}

        this.centerX = this.scene.scale.width * .5;
        this.centerY = this.scene.scale.height * .5;
        this.width = this.scene.scale.width;
        this.height = this.scene.scale.height;

    }

    create = () => {
        
    }
}