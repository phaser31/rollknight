import { EventEmitter } from "events";

export default class InputView {

    /**
     * @param  {Phaser.Scene} scene
     */
    constructor(scene) {
        this.scene = scene;

        this.container = this.scene.add.container();

        this.event = new EventEmitter();
		this.eventNames = {
		}

        this.centerX = this.scene.scale.width * .5;
        this.centerY = this.scene.scale.height * .5;
        this.width = this.scene.scale.width;
        this.height = this.scene.scale.height;

    }

    create = () => {
        this.line = this.scene.add.line(0,0,0,0,0,0,0xffffff)
        this.line.setOrigin(0,0);
		this.line.setLineWidth(this.width * .003);
		this.line.setAlpha(0.5);
    }

    moveLine = (x1, y1, x2, y2) => {
        this.line.setTo(x1, y1, x2, y2);
    }

    setVisible = (visible) => {
        let alpha = visible ? 0.5 : 0;
        // this.line.setAlpha(alpha);
        this.line.setVisible(visible);
    }
}