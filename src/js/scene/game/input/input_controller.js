import { EventEmitter } from "events";
import game_config from "../../../data/game_config";
import InputView from "./input_view";

export default class InputController {
    /**
     * @param  {Phaser.Scene} scene
     */
    constructor(scene) {
        this.scene = scene;

        this.event = new EventEmitter();
        this.eventNames = {
            onInputReleased: "onInputReleased"
        }

        this.enable = true;
        this.angleMin = game_config.ANGLE_MIN;
        this.angleMax = game_config.ANGLE_MAX;

        this.isPointerDown = false;
    }

    init = () => {
        this.view = new InputView(this.scene);
        this.view.create();
    }

    setBallPosition = (x, y) => {
        this.ballX = x;
        this.ballY = y;
    }

    setBorderPosition = (upperBorderY, lowerBorderY, leftBorderX, rightBorderX) => {
        this.upperBorderY = upperBorderY;
        this.lowerBorderY = lowerBorderY;
        this.leftBorderX = leftBorderX;
        this.rightBorderX = rightBorderX;
    }

    setBorderSize = (width, height) => {
        this.boardWidth = width;
        this.boardHeight = height;
    }

    updateLine = (x1, y1, x2, y2) => {
        const adjustedLine = this.adjustLine(x1, y1, x2, y2);
        this.view.moveLine(adjustedLine.x1, adjustedLine.y1, adjustedLine.x2, adjustedLine.y2);
        return adjustedLine;
    }

    adjustLine = (x1, y1, x2, y2) => {
        let rad = Phaser.Math.Angle.Between(x1, y1, x2, y2);
        let angle = Phaser.Math.RadToDeg(rad);

        let x3 = x2;
        let y3 = y2;

        let acceptAngle = angle >= -this.angleMax && angle <= -this.angleMin;
        if (!acceptAngle) {
            let newAngle;
            if (angle < -this.angleMax)
                newAngle = (180 + angle) + (180 + this.angleMin);
            else
                newAngle = angle + this.angleMin;
            let angleRatio = ((this.angleMax - this.angleMin) / (360 - (this.angleMax - this.angleMin)));
            newAngle = -(newAngle * angleRatio + this.angleMin);
            rad = Phaser.Math.DegToRad(newAngle);
            x3 = this.boardHeight * Math.cos(rad) + x1;
            y3 = this.boardHeight * Math.sin(rad) + y1;
        }

        let y = y1 - this.boardHeight;
        let x = ((x3 - x1) * (y3 - y) - x3 * (y3 - y1)) / (y1 - y3);

        if (x > this.view.centerX + (this.boardWidth / 2)) {
            x = this.view.centerX + (this.boardWidth / 2);
            y = (y3 * (x3 - x1) - (y3 - y1) * (x3 - x)) / (x3 - x1);
        }
        else if (x < this.view.centerX - (this.boardWidth / 2)) {
            x = this.view.centerX - (this.boardWidth / 2);
            y = (y3 * (x3 - x1) - (y3 - y1) * (x3 - x)) / (x3 - x1);
        }

        return {
            x1,
            y1,
            x2: x,
            y2: y
        }
    }

    createInputEvent = () => {
        let line = {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 0
        }
        this.scene.input.on('pointerdown', pointer => {
            if (!this.enable || this.isPointerDown) return;
            this.isPointerDown = true;
            this.view.setVisible(true);
            line = this.updateLine(this.ballX, this.ballY, pointer.x, pointer.y);
        });
        this.scene.input.on('pointerup', () => {
            if (!this.enable || !this.isPointerDown) return;
            this.view.setVisible(false);
            const target = {
                x: line.x2 - line.x1,
                y: line.y2 - line.y1,
            }
            this.event.emit(this.eventNames.onInputReleased, target);
            this.enable = false;
            this.isPointerDown = false;
        })
        this.scene.input.on('pointermove', pointer => {
            if (!this.enable || !this.isPointerDown) return;
            line = this.updateLine(this.ballX, this.ballY, pointer.x, pointer.y);
        })
    }

    setEnableInput = (enable) => {
        this.enable = enable;
    }
}