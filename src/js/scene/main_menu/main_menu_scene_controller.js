import { Scene_Info } from "../../data/game_scene";
import MainMenuSceneView from "./main_menu_scene_view";

export default class MainMenuSceneController extends Phaser.Scene {
    constructor() {
        super({
            key: Scene_Info.mainMenu
        });
    }

    create = () => {
        this.view = new MainMenuSceneView(this);
        this.view.create();

        this.input.on('pointerdown', () => {
            this.view.removeAnimation();
            this.scene.start(Scene_Info.game);
        });
    }
}