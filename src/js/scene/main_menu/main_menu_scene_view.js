import { EventEmitter } from "events";
import { Assets } from "../../data/game_assets";

export default class MainMenuSceneView {

    /**
     * @param  {Phaser.Scene} scene
     */
    constructor(scene) {
        this.scene = scene;

        this.container = this.scene.add.container();

        this.event = new EventEmitter();
		this.eventNames = {
		}

        this.centerX = this.scene.scale.width * .5;
        this.centerY = this.scene.scale.height * .5;
        this.width = this.scene.scale.width;
        this.height = this.scene.scale.height;

        this.animKey = {
            menuIdle: 'menuIdle'
        }

    }

    create = () => {
        this.background = this.scene.add.image(this.centerX, this.centerY, Assets.loading.images.menu.key);
        const backgroundWidth = this.height * (this.background.width / this.background.height);
        this.background.setDisplaySize(backgroundWidth, this.height);

        this.lightBackground = this.scene.add.rectangle(this.centerX, this.centerY, this.width, this.height);
        this.lightBackground.setFillStyle(0xffffff);
        this.lightBackground.setAlpha(.1);

        this.playerAnim = this.scene.anims.create({
            key: this.animKey.menuIdle,
            frames: Assets.spritesheets.playerIdle.key,
            frameRate: 12,
            repeat: -1,
        });
        this.player = this.scene.add.sprite(this.centerX, this.centerY + this.width * .1, Assets.spritesheets.playerIdle.key, 0);
        const playerWidth = this.width * .9
        this.player.setDisplaySize(playerWidth, (playerWidth) * (this.player.height/this.player.width));
        this.player.anims.play(this.animKey.menuIdle);

        this.lightBackground = this.scene.add.rectangle(this.centerX, this.centerY - this.height * .35, this.width, this.height * .25);
        this.lightBackground.setFillStyle(0xffffff);
        this.lightBackground.setAlpha(.5);

        this.titleText = this.scene.add.text(this.player.x, this.player.y - this.player.displayHeight * .9, "Roll", {
            align: "center",
            fontSize: this.player.displayHeight * .3,
            color: "#353540",
            fontFamily: Assets.loading.fonts.origamiMommy.key,
            wordWrap: {
                width: this.width * .9,
            }
        });        
        this.titleText.setOrigin(.5);

        this.subtitleText = this.scene.add.text(this.player.x, this.player.y - this.player.displayHeight * .7, "Knight", {
            align: "center",
            fontSize: this.player.displayHeight * .2,
            color: "#353540",
            fontFamily: Assets.loading.fonts.origamiMommy.key,
            wordWrap: {
                width: this.width * .9,
            }
        });        
        this.subtitleText.setOrigin(.5);
        
        this.playText = this.scene.add.text(this.player.x, this.player.y + this.player.displayHeight * .5, "Tap to Play", {
            align: "center",
            fontSize: this.player.displayHeight * .1,
            color: "#FFFFFF",
            fontFamily: Assets.loading.fonts.origamiMommy.key,
            wordWrap: {
                width: this.width * .9,
            }
        });
        this.tween = this.scene.add.tween({
            targets: this.playText,
            duration: 750,
            alpha: 0,
            yoyo: true,
            ease: 'Linear',
            repeat: -1
        });
        this.playText.setOrigin(.5);
    }

    removeAnimation = () => {
        this.scene.anims.remove(this.animKey.menuIdle);
    }
}