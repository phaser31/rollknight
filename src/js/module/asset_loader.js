export default class AssetLoader {

    static LoadSpriteSheet(scene, info) {
        scene.load.spritesheet(
            info.key,
            info.path,
            { frameWidth: info.width, frameHeight: info.height }
        )
    }

    static LoadImage(scene, info) {
        scene.load.image(info.key, info.path);
    }

    static LoadFont(key, path) {
        return new Promise((resolve, reject) => {
            const newFont = new FontFace(key, `url(${path})`);
            newFont.load().then(function (loaded) {
                document.fonts.add(loaded);
                resolve();
            }).catch(function (error) {
                reject(error)
            });
        })
    }

}