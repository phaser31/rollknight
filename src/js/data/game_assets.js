import map from '../../assets/background/map.png'
import menu from '../../assets/background/menu.png'

import dust from '../../assets/human/dust.png'
import playerRun from '../../assets/human/lancelot/img3.png';
import playerIdle from '../../assets/human/lancelot/img1.png';
import playerRoll from '../../assets/human/lancelot/img13b.png';
import playerDie from '../../assets/human/lancelot/img19.png';
import goblinIdle from '../../assets/enemy/goblin/img1.png';
import goblinHurt from '../../assets/enemy/goblin/img13.png';
import goblinDie from '../../assets/enemy/goblin/img15.png';
import hobgoblinIdle from '../../assets/enemy/hobgoblin/img1.png';
import hobgoblinHurt from '../../assets/enemy/hobgoblin/img13.png';
import hobgoblinDie from '../../assets/enemy/hobgoblin/img15.png';
import golemIdle from '../../assets/enemy/golem/img1.png';
import golemHurt from '../../assets/enemy/golem/img17.png';
import golemDie from '../../assets/enemy/golem/img19.png';

import origamiMommy from '../../assets/font/origami_mommy.ttf';

export const Assets = {
    loading: {
        images: {
            menu: {
                key: 'menu',
                path: menu,
            },
        },
        spritesheets: {
            playerRun: {
                key: 'playerRun',
                path: playerRun,
                width: 128 / 4,
                height: 32 / 1,
            },
            dust: {
                key: 'dust',
                path: dust,
                width: 48 / 6,
                height: 8 / 1,
            },
        },
        fonts: {
            origamiMommy: {
                key: 'origamiMommy',
                path: origamiMommy
            }
        }
    },
    images: {
        map: {
            key: 'map',
            path: map,
        },
    },
    spritesheets: {
        playerIdle: {
            key: 'playerIdle',
            path: playerIdle,
            width: 128 / 4,
            height: 32 / 1,
        },
        playerRoll: {
            key: 'playerRoll',
            path: playerRoll,
            width: 128 / 4,
            height: 32 / 1,
        },
        playerDie: {
            key: 'playerDie',
            path: playerDie,
            width: 128 / 4,
            height: 32 / 1,
        },
        goblinIdle: {
            key: 'goblinIdle',
            path: goblinIdle,
            width: 128 / 4,
            height: 32 / 1,
        },
        goblinHurt: {
            key: 'goblinHurt',
            path: goblinHurt,
            width: 128 / 4,
            height: 32 / 1,
        },
        goblinDie: {
            key: 'goblinDie',
            path: goblinDie,
            width: 128 / 4,
            height: 32 / 1,
        },
        hobgoblinIdle: {
            key: 'hobgoblinIdle',
            path: hobgoblinIdle,
            width: 128 / 4,
            height: 32 / 1,
        },
        hobgoblinHurt: {
            key: 'hobgoblinHurt',
            path: hobgoblinHurt,
            width: 128 / 4,
            height: 32 / 1,
        },
        hobgoblinDie: {
            key: 'hobgoblinDie',
            path: hobgoblinDie,
            width: 128 / 4,
            height: 32 / 1,
        },
        golemIdle: {
            key: 'golemIdle',
            path: golemIdle,
            width: 128 / 4,
            height: 32 / 1,
        },
        golemHurt: {
            key: 'golemHurt',
            path: golemHurt,
            width: 128 / 4,
            height: 32 / 1,
        },
        golemDie: {
            key: 'golemDie',
            path: golemDie,
            width: 128 / 4,
            height: 32 / 1,
        },
    },
    fonts: {}
}