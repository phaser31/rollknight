export default {
    BOARD_WIDTH: 9,
    BOARD_HEIGHT: 12,

    MIN_BLOCK_SPAWN: 2,
    MAX_BLOCK_SPAWN: 8,

    ANGLE_MIN: 3,
    ANGLE_MAX: 177
}