import Phaser from 'phaser';
import './css/index.css'
import GameSceneController from './js/scene/game/game_scene_controller';
import LoadingSceneController from './js/scene/loading/loading_scene_controller';
import MainMenuSceneController from './js/scene/main_menu/main_menu_scene_controller';

function getGameSize() {
    let config = {
        width: window.innerWidth,
        height: window.innerHeight,
        zoom: 1,
    }

    //check landscape or larger than 9:16 aspect ratio
    if ((config.width/config.height) > (9/16)) {
        config.width = config.height * (9/16);
    }

    return config;
}

const screenProfile = getGameSize();

const config = {
    type: Phaser.AUTO,
    parent: 'game',
    backgroundColor: '#111111',
    scene: [LoadingSceneController, MainMenuSceneController, GameSceneController],
    scale: {
        mode: Phaser.Scale.NONE,
        // autoCenter: Phaser.Scale.Center.CENTER_HORIZONTALLY,
        width: screenProfile.width,
        height: screenProfile.height,
        zoom: screenProfile.zoom,
    },
    dom: {
        createContainer: true
    },
    render: {
        antiAlias: true,
        pixelArt: true,
        roundPixels: true
    },
    physics: {
        default: 'arcade',
        arcade: {
        //    x: 0,
        //    y: 0,
        //    width: scene.sys.scale.width,
        //    height: scene.sys.scale.height,
        //    gravity: {
        //        x: 0,
        //        y: 0
        //    },
        //    checkCollision: {
        //        up: true,
        //        down: true,
        //        left: true,
        //        right: true
        //    },
        //    customUpdate: false,
        //    fixedStep: true,
        //    fps: 60,
        //    timeScale: 1,     // 2.0 = half speed, 0.5 = double speed
        //    customUpdate: false,
        //    overlapBias: 4,
        //    tileBias: 16,
        //    forceX: false,
        //    isPaused: false,
        //    debug: true,
        //    debugShowBody: true,
        //    debugShowStaticBody: true,
        //    debugShowVelocity: true,
        //    debugBodyColor: 0xff00ff,
        //    debugStaticBodyColor: 0x0000ff,
        //    debugVelocityColor: 0x00ff00,
        //    maxEntries: 16,
        //    useTree: true   // set false if amount of dynamic bodies > 5000
        }
    },
    plugins:{},
    autoRound: false,
};

const game = new Phaser.Game(config);
